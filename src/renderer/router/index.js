import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'landing-page',
      component: require('@/components/LandingPage').default
    },
    {
      path: '/reddit-post',
      name: 'reddit-post-page',
      component: require('@/components/RedditPostPage').default
    },
    {
      path: '/reddit-post/form',
      name: 'reddit-post-form-page',
      component: require('@/components/RedditPostFormPage').default
    },
    {
      path: '/utilities',
      name: 'utilities-page',
      component: require('@/components/UtilitiesPage').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
