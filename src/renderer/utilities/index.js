const nFormatter = (num, digits) => {
    const si = [{
      value: 1,
      symbol: ''
    },
    {
      value: 1e3,
      symbol: 'k'
    },
    {
      value: 1e6,
      symbol: 'M'
    },
    {
      value: 1e9,
      symbol: 'G'
    },
    {
      value: 1e12,
      symbol: 'T'
    },
    {
      value: 1e15,
      symbol: 'P'
    },
    {
      value: 1e18,
      symbol: 'E'
    }
    ]
    const rx = /\.0+$|(\.[0-9]*[1-9])0+$/
    let i
    for (i = si.length - 1; i > 0; i--) {
      if (num >= si[i].value) {
        break
      }
    }
    return (num / si[i].value).toFixed(digits).replace(rx, '$1') + si[i].symbol
  }
  
  async function asyncForEach (array, callback) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array)
    }
  }
  
  const fixSVGAttributes = (el) => {
    function recurseElementChildren (node) {
      const transformProperties = [
        'fill',
        'color',
        'font-size',
        'stroke',
        'font'
      ]
  
      if (!node.style) return
  
      let styles = getComputedStyle(node)
  
      for (let transformProperty of transformProperties) {
        node.style[transformProperty] = styles[transformProperty]
      }
  
      for (let child of Array.from(node.childNodes)) {
        recurseElementChildren(child)
      }
    }
  
    let svgElems = Array.from(el.getElementsByTagName('svg'))
  
    for (let svgElement of svgElems) {
      recurseElementChildren(svgElement)
    }
  }
  
  function makeid (length) {
    var result = ''
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    var charactersLength = characters.length
  
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength))
    }
  
    return result
  }

  function htmlDecode(input){
    var e = document.createElement('div');
    e.innerHTML = input;
    return e.childNodes.length === 0 ? "" : e.childNodes[0].nodeValue;
  }
  
  function removeComment(input) {
    return input
        .replace(/<!--(.+)-->/g, '')
        .replace(/<div class="md">/g, '')
        .replace(/<\/div>/g, '');
  }

  export {
    nFormatter,
    asyncForEach,
    fixSVGAttributes,
    makeid,
    htmlDecode,
    removeComment,
  }
  