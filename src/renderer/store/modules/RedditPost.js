const state = {
  posts: [],
  audio: {
    file: '',
    base64: '',
  }
}

const mutations = {
  SET_POSTS (state, value) {
    state.posts = value;
  },
  SET_AUDIO (state, value) {
    state.audio = value;
  },
  SET_PARAGRAPHS(state, {post, p}) {
    const sPost = state.posts.find(p => p.id === post.id);
    sPost.p = p;
  },
  UPDATE_POST(state, {post, value}) {
    state.posts = state.posts.map(p => {
      if (p.id === post.id) {
        return {...p, ...value};
      }

      return p;
    });
  },
  UPDATE_PARAGRAPH(state, {post, p, value}) {
    let sPost = state.posts.find(p => p.id === post.id)
    sPost.p = sPost.p.map(pa => {
      if (pa.id === p.id) {
        return {
          ...pa,
          ...value,
        };
      }

      return pa;
    });
  },
}

const actions = {
  onSetPost({commit}, value) {
    commit('SET_POSTS', value);
  },
  onSetParagraphs({commit}, value) {
    commit('SET_PARAGRAPHS', value);
  },
  onSetAudio({commit}, value) {
    commit('SET_AUDIO', value);
  },
  onUpdatePost({commit}, value) {
    commit('UPDATE_POST', value);
  },
  onUpdateParagraph({commit}, value) {
    commit('UPDATE_PARAGRAPH', value);
  }
}

const getters = {
  posts(state) {
    return state.posts;
  },
  audio(state) {
    return state.audio;
  },
  allAudioText(state) {
    const audioText = [];

    state.posts.forEach((post) => {
      audioText.push(post.audio);

      post.p.forEach((p) => {
        audioText.push(p.audio);
      });
    })

    return audioText.join(' ');
  },
  postsAudio(state) {
    const audios = [];

    state.posts.forEach((post) => {
      const audio = {
        text: '',
        after: post.postCredit.audio,
      };

      const text = [];
      
      text.push(post.audio);

      post.p.forEach((p) => {
        text.push(p.audio);
      });
      
      audio.text = text.join(' ');
      
      audios.push(audio);
    });

    return audios;
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
}
