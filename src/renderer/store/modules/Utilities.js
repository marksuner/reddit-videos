const state = {
  folder: 'C:\\Users\\MarkSuner\\Desktop\\Videos',
  voice: '',
  volume: 1,
  postCreditImage: '',
}

const mutations = {
  SET_FOLDER (state, value) {
    state.folder = value;
  },
  SET_VOICE (state, value) {
    state.voice = value;
  },
  SET_VOLUME (state, value) {
    state.volume = value;
  },
  SET_POST_CREDIT_IMAGE (state, value) {
    state.postCreditImage = value;
  },
}

const actions = {
  onSetFolder({commit}, value) {
    commit('SET_FOLDER', value);
  },
  onSetVoice({commit}, value) {
    commit('SET_VOICE', value);
  },
  onSetVolume({commit}, value) {
    commit('SET_VOLUME', value);
  },
  onSetPostCreditImage({commit}, value) {
    commit('SET_POST_CREDIT_IMAGE', value);
  }
}

const getters = {
  folder(state) {
    return state.folder;
  }, 
  voice(state) {
    return state.voice;
  },
  volume(state) {
    return state.volume;
  },
  postCreditImage(state) {
    return state.postCreditImage
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
}
