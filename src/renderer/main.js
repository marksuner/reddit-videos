import 'bootstrap/dist/css/bootstrap.min.css'
import './main.css'

import 'babel-polyfill'
import 'reddit.js';

import Vue from 'vue'
import axios from 'axios'
import VTooltip from 'v-tooltip'
import VueHtml2Canvas from 'vue-html2canvas';
import Notifications from 'vue-notification'

import App from './App'
import router from './router'
import store from './store'

export const bus = new Vue();

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))

Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false

Vue.use(VueHtml2Canvas);
Vue.use(VTooltip)
Vue.use(Notifications)

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  store,
  template: '<App/>'
}).$mount('#app')
